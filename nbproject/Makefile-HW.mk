#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-MacOSX
CND_DLIB_EXT=dylib
CND_CONF=HW
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/sil/bas.o \
	${OBJECTDIR}/sil/dis.o \
	${OBJECTDIR}/sil/hal.o \
	${OBJECTDIR}/sil/kos.o \
	${OBJECTDIR}/sil/mdc.o \
	${OBJECTDIR}/sil/mst.o \
	${OBJECTDIR}/sil/nvm.o \
	${OBJECTDIR}/sil/ros.o \
	${OBJECTDIR}/sil/tas.o \
	${OBJECTDIR}/sil/wrp.o \
	${OBJECTDIR}/tinkerforge/brick_dc.o \
	${OBJECTDIR}/tinkerforge/brick_master.o \
	${OBJECTDIR}/tinkerforge/brick_red.o \
	${OBJECTDIR}/tinkerforge/brick_stepper.o \
	${OBJECTDIR}/tinkerforge/bricklet_io16.o \
	${OBJECTDIR}/tinkerforge/bricklet_io4.o \
	${OBJECTDIR}/tinkerforge/bricklet_segment_display_4x7.o \
	${OBJECTDIR}/tinkerforge/ip_connection.o \
	${OBJECTDIR}/wl.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lpthread

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/wiliti_sil_et503

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/wiliti_sil_et503: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	gcc -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/wiliti_sil_et503 ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/sil/bas.o: sil/bas.c 
	${MKDIR} -p ${OBJECTDIR}/sil
	${RM} "$@.d"
	$(COMPILE.c) -g -DHW -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/sil/bas.o sil/bas.c

${OBJECTDIR}/sil/dis.o: sil/dis.c 
	${MKDIR} -p ${OBJECTDIR}/sil
	${RM} "$@.d"
	$(COMPILE.c) -g -DHW -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/sil/dis.o sil/dis.c

${OBJECTDIR}/sil/hal.o: sil/hal.c 
	${MKDIR} -p ${OBJECTDIR}/sil
	${RM} "$@.d"
	$(COMPILE.c) -g -DHW -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/sil/hal.o sil/hal.c

${OBJECTDIR}/sil/kos.o: sil/kos.c 
	${MKDIR} -p ${OBJECTDIR}/sil
	${RM} "$@.d"
	$(COMPILE.c) -g -DHW -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/sil/kos.o sil/kos.c

${OBJECTDIR}/sil/mdc.o: sil/mdc.c 
	${MKDIR} -p ${OBJECTDIR}/sil
	${RM} "$@.d"
	$(COMPILE.c) -g -DHW -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/sil/mdc.o sil/mdc.c

${OBJECTDIR}/sil/mst.o: sil/mst.c 
	${MKDIR} -p ${OBJECTDIR}/sil
	${RM} "$@.d"
	$(COMPILE.c) -g -DHW -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/sil/mst.o sil/mst.c

${OBJECTDIR}/sil/nvm.o: sil/nvm.c 
	${MKDIR} -p ${OBJECTDIR}/sil
	${RM} "$@.d"
	$(COMPILE.c) -g -DHW -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/sil/nvm.o sil/nvm.c

${OBJECTDIR}/sil/ros.o: sil/ros.c 
	${MKDIR} -p ${OBJECTDIR}/sil
	${RM} "$@.d"
	$(COMPILE.c) -g -DHW -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/sil/ros.o sil/ros.c

${OBJECTDIR}/sil/tas.o: sil/tas.c 
	${MKDIR} -p ${OBJECTDIR}/sil
	${RM} "$@.d"
	$(COMPILE.c) -g -DHW -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/sil/tas.o sil/tas.c

${OBJECTDIR}/sil/wrp.o: sil/wrp.c 
	${MKDIR} -p ${OBJECTDIR}/sil
	${RM} "$@.d"
	$(COMPILE.c) -g -DHW -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/sil/wrp.o sil/wrp.c

${OBJECTDIR}/tinkerforge/brick_dc.o: tinkerforge/brick_dc.c 
	${MKDIR} -p ${OBJECTDIR}/tinkerforge
	${RM} "$@.d"
	$(COMPILE.c) -g -DHW -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tinkerforge/brick_dc.o tinkerforge/brick_dc.c

${OBJECTDIR}/tinkerforge/brick_master.o: tinkerforge/brick_master.c 
	${MKDIR} -p ${OBJECTDIR}/tinkerforge
	${RM} "$@.d"
	$(COMPILE.c) -g -DHW -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tinkerforge/brick_master.o tinkerforge/brick_master.c

${OBJECTDIR}/tinkerforge/brick_red.o: tinkerforge/brick_red.c 
	${MKDIR} -p ${OBJECTDIR}/tinkerforge
	${RM} "$@.d"
	$(COMPILE.c) -g -DHW -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tinkerforge/brick_red.o tinkerforge/brick_red.c

${OBJECTDIR}/tinkerforge/brick_stepper.o: tinkerforge/brick_stepper.c 
	${MKDIR} -p ${OBJECTDIR}/tinkerforge
	${RM} "$@.d"
	$(COMPILE.c) -g -DHW -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tinkerforge/brick_stepper.o tinkerforge/brick_stepper.c

${OBJECTDIR}/tinkerforge/bricklet_io16.o: tinkerforge/bricklet_io16.c 
	${MKDIR} -p ${OBJECTDIR}/tinkerforge
	${RM} "$@.d"
	$(COMPILE.c) -g -DHW -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tinkerforge/bricklet_io16.o tinkerforge/bricklet_io16.c

${OBJECTDIR}/tinkerforge/bricklet_io4.o: tinkerforge/bricklet_io4.c 
	${MKDIR} -p ${OBJECTDIR}/tinkerforge
	${RM} "$@.d"
	$(COMPILE.c) -g -DHW -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tinkerforge/bricklet_io4.o tinkerforge/bricklet_io4.c

${OBJECTDIR}/tinkerforge/bricklet_segment_display_4x7.o: tinkerforge/bricklet_segment_display_4x7.c 
	${MKDIR} -p ${OBJECTDIR}/tinkerforge
	${RM} "$@.d"
	$(COMPILE.c) -g -DHW -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tinkerforge/bricklet_segment_display_4x7.o tinkerforge/bricklet_segment_display_4x7.c

${OBJECTDIR}/tinkerforge/ip_connection.o: tinkerforge/ip_connection.c 
	${MKDIR} -p ${OBJECTDIR}/tinkerforge
	${RM} "$@.d"
	$(COMPILE.c) -g -DHW -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tinkerforge/ip_connection.o tinkerforge/ip_connection.c

${OBJECTDIR}/wl.o: wl.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -DHW -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/wl.o wl.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/wiliti_sil_et503

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
