/* 
 * | ---------------------------------------------------------------- |
 * | WindowLifter | Software In The Loop                              |
 * | Version 1.0, 09.01.2016                                          | 
 * | Carsten Beyer, Jan Emmerich                                      |
 * |                                                                  |
 * | Modul ET551: Master-Projekt 2                                    |
 * | „Software In The Loop“ zur Unterstützung des Software-           |
 * | Entwicklungsprozesses am Beispiel des WindowLifter-Modells       |
 * | ---------------------------------------------------------------- |
 */

/* Includes */
#include "stdlib.h"

#include "sil/wrp.h"
#include "sil/ros.h"
#include "sil/mdc.h"

/* Static function declaration */
static void user_app(void);

/* Main */
int main(int argc, char** argv)
{
    wrp_init();

    //user app
    user_app();

    wrp_shutdown();

    return (EXIT_SUCCESS);
}

/* Static function definition */
static void user_app(void)
{
    ros_init();
    mdc_init();

    mdc_set_speed(MDC_SPEED_MIDDLE);
    mdc_on();

    while (1)
    {
        switch (ros_get_position())
        {
            case 1:
                mdc_do_cmd(MDC_DIR_UP);
                break;
            case 0:
                mdc_do_cmd(MDC_DIR_STOP);
                break;
            case -1:
                mdc_do_cmd(MDC_DIR_DOWN);
                break;
            default:
                /* ToDo: Handle default state */
                break;
        }
    }
}