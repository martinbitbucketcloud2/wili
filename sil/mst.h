#ifndef MST_H
#define MST_H

#include <stdint.h>

/**
 * - direction-commands for the Stepper
 */
typedef enum MstDir_ {
    MST_DIR_UP,
    MST_DIR_DOWN,
    MST_DIR_STOP
} MstDir;

/**
 * - velocity-commands for the Motor Driver
 */
typedef enum MstSpeed_ {
    MST_SPEED_LOW,
    MST_SPEED_MID,
    MST_SPEED_HIGH
} MstSpeed;

extern int mst_init(void);
extern void mst_shutdown(void);

extern void mst_on(void);
extern void mst_off(void);

extern void mst_do_cmd(MstDir dir);
extern void mst_do_steps(MstDir dir, int32_t steps);
extern void mst_set_speed(MstSpeed spe);
extern int mst_get_consumption(void);
extern int mst_get_steps(void);

#endif /* MST_H */

