#ifndef CONSTANTS_H
#define CONSTANTS_H

/**
 * - defines IP_ADDR and PORT for connecting to the UI
 */
#define IP_ADDR "localhost"
#define PORT_UI 54321

/**
 * - defines HOST and PORT for connecting to the Bricks
 */
#define LIFTER_HOST "192.168.42.1"
#define HANDHELD_HOST "192.168.42.2"
#define PORT_LIFTER 4223

/**
 * - defines the UID of the Bricklets
 */
#define DC_UID "6CNVGv"
#define MST_UID "67Qo5y"
#define IO16_UID "vZX"
#define IO4_UID "gX7"
#define SEG_UID "iUC"

/**
 * - defines the name of the file for saving and restoring the window-position
 */
#define FILENAME "nvm.txt"

/**
 * - defines the maximum lenght of the message, which is send to the UI
 */
#define MSG_LEN 18

#endif /* CONSTANTS_H */