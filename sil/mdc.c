/* 
 * - dc-motor
 */

#include <stdio.h>
#include <stdlib.h>

#include "../tinkerforge/brick_dc.h"
#include "../tinkerforge/bricklet_io16.h"
#include "../tinkerforge/ip_connection.h"

#include "mdc.h"

#include "constants.h"
#include "wrp.h"

#ifdef HW
/**
 * - defines an "entity" of the DC-Brick
 */
static DC mdc;

/**
 * - defines an "entity" of the IPConnection
 */
IPConnection ipcon_mdc;
#endif

/**
 * - saves the current speed-settings
 */
#ifdef HW
static int mdc_speed_up = 0;
static int mdc_speed_down = 0;
#endif

/**
 * - initializes the DC by connecting to the DC-Brick
 *  - sets the drive mode, the pwm frequency, the accelaration and the initial
 * velocity
 */
extern int mdc_init(void)
{
    int status = 0;

#ifdef HW
    ipcon_create(&ipcon_mdc);


    dc_create(&mdc, DC_UID, &ipcon_mdc);

    if (ipcon_connect(&ipcon_mdc, LIFTER_HOST, PORT_LIFTER) < 0)
    {
        printf("Could not connect [mdc: DC-Brick]\n");
        status = 1;
    }
    else
    {
        status = 0;
    }

    dc_set_drive_mode(&mdc, DC_DRIVE_MODE_DRIVE_BRAKE);
    dc_set_pwm_frequency(&mdc, 15000);
    dc_set_acceleration(&mdc, 65000);
    dc_set_velocity(&mdc, 0);
#endif

    return status;
}

/**
 * - disconnects the motor by destroying the "entities" of the DC-Brick and 
 * the IPConnection
 */
extern void mdc_shutdown(void)
{
    mdc_off();
#ifdef HW
    dc_destroy(&mdc);
    ipcon_destroy(&ipcon_mdc);
#endif
}

/**
 * - enables the motor
 */
extern void mdc_on(void)
{
#ifdef HW
    dc_enable(&mdc);
#endif

#ifdef SW
    wrp_set_data(WRP_MDC_ENABLE_ON);
#endif
}

/**
 * - disables the motor
 */
extern void mdc_off(void)
{
#ifdef HW
    dc_disable(&mdc);
#endif

#ifdef SW
    wrp_set_data(WRP_MDC_ENABLE_OFF);
#endif
}

/**
 * - sets the direction of the dc-motor
 */
extern void mdc_do_cmd(MdcDir dir)
{
#ifdef HW
    int velocity = 0;
#endif
    switch (dir)
    {
        case MDC_DIR_STOP:
#ifdef HW
            velocity = 0;
#endif

#ifdef SW
            wrp_set_data(WRP_MDC_DIR_STOP);
#endif
            break;

        case MDC_DIR_UP:
#ifdef HW
            velocity = mdc_speed_up;
#endif

#ifdef SW
            wrp_set_data(WRP_MDC_DIR_UP);
#endif
            break;

        case MDC_DIR_DOWN:
#ifdef HW
            velocity = mdc_speed_down;
#endif

#ifdef SW
            wrp_set_data(WRP_MDC_DIR_DOWN);
#endif
            break;

        default:
#ifdef HW
            velocity = 0;
            dc_full_brake(&mdc);
#endif

#ifdef SW
            wrp_set_data(WRP_MDC_DIR_STOP);
#endif
            break;
    }

#ifdef HW
    dc_set_velocity(&mdc, velocity);
#endif
}

/*
 * - sets the speed of the dc-motor depending on the commited value
 */
extern void mdc_set_speed(MdcSpeed speed)
{
    switch (speed)
    {
        case MDC_SPEED_LOW:
#ifdef HW
            mdc_speed_up = -8200;
            mdc_speed_down = 6000;
#endif

#ifdef SW
            wrp_set_data(WRP_MDC_SPEED_LOW);
#endif
            break;

        case MDC_SPEED_MIDDLE:
#ifdef HW
            mdc_speed_up = -8700;
            mdc_speed_down = 6200;
#endif

#ifdef SW
            wrp_set_data(WRP_MDC_SPEED_MIDDLE);
#endif
            break;

        case MDC_SPEED_HIGH:
#ifdef HW
            mdc_speed_up = -10050;
            mdc_speed_down = 6700;
#endif

#ifdef SW
            wrp_set_data(WRP_MDC_SPEED_HIGH);
#endif
            break;

        default:
#ifdef HW
            mdc_speed_up = 0;
            mdc_speed_down = 0;
#endif
            break;
    }
}

/**
 * - reads the current consumption of the dc-motor
 */
extern int mdc_get_consumption(void)
{
#ifdef HW
    uint16_t current_consumption = 0;

    dc_get_current_consumption(&mdc, &current_consumption);
#endif

#ifdef SW
    int current_consumption = 0;
    wrp_get_data(WRP_MDC_CURRENT, &current_consumption);
#endif

    return (int) current_consumption;
}