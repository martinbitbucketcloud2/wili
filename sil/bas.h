#ifndef BAS_H
#define BAS_H

extern int bas_init(void);
extern void bas_shutdown(void);

extern int bas_get_state(void);

#endif /* BAS_H */

