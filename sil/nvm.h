#ifndef NVM_H
#define NVM_H

extern void nvm_write_value(int value);
extern int nvm_read_value(void);

#endif /* NVM_H */