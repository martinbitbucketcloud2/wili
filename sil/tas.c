/* 
 * - top-snap-action-switch
 */

#include <stdio.h>

#include "../tinkerforge/ip_connection.h"
#include "../tinkerforge/bricklet_io16.h"

#include "tas.h"

#include "constants.h"
#include "wrp.h"

#ifdef HW
/**
 * - defines an "entity" of the IO-16-Bricklet and an IPConnection
 */
static IO16 io;
static IPConnection ipcon;
#endif

/**
 * - initializes the Top Sensor by connecting to the IO-16-Bricklet
 */
extern int tas_init(void)
{
    int status = 0;

#ifdef HW
    ipcon_create(&ipcon);

    io16_create(&io, IO16_UID, &ipcon);

    if (ipcon_connect(&ipcon, LIFTER_HOST, PORT_LIFTER) < 0)
    {
        printf("Could not connect [tas: IO-Bricklet]\n");
        status = 1;
    }
    else
    {
        status = 0;
    }
#endif


    return status;
}

/**
 * - disconnects the top snap-action switch by destroying the "entities" of 
 * the IO-16-Bricklet and the IPConnection
 */
extern void tas_shutdown(void)
{
#ifdef HW
    io16_destroy(&io);
    ipcon_destroy(&ipcon);
#endif

}

/**
 * - checks and returns, whether the bottom snap-action switch is pressed or not
 */
extern int tas_get_state(void)
{
    int tas_state = 0;

#ifdef HW
    uint8_t value_mask_bps = 0x00;

    if (io16_get_port(&io, 'a', &value_mask_bps) < 0)
    {
        printf("Could not get value from port A as bitmask, probably timeout"
            "[tas: IO-Bricklet]\n");
    }
    else
    {

    }

    if (0x00 == (value_mask_bps & 0x04))
    {
        tas_state = 1;
    }
    else
    {
        tas_state = 0;
    }
#endif

#ifdef SW
    wrp_get_data(WRP_TAS_STATE, &tas_state);
#endif

    return tas_state;
}