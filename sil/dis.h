#ifndef DIS_H
#define DIS_H

extern int dis_init(void);
extern void dis_shutdown(void);

extern void dis_show_value(int value);
extern void dis_turn_off(void);

#endif /* DIS_H */

