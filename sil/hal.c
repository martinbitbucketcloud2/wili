/*
 * - hall-sensor
 */

#include <stdio.h>
#include <stdlib.h>

#include "../tinkerforge/bricklet_io16.h"
#include "../tinkerforge/ip_connection.h"

#include "hal.h"

#include "constants.h"
#include "wrp.h"
#include "bas.h"
#include "tas.h"

#ifdef HW
/**
 * - defines an "entity" of the IO-16-Bricklet
 */
static IO16 io;

/**
 * - defines an "entity" of the IPConnection
 */
IPConnection ipcon;
#endif

extern int hal_init(void)
{
    int status = 0;

#ifdef HW
    ipcon_create(&ipcon);

    io16_create(&io, IO16_UID, &ipcon);

    if (ipcon_connect(&ipcon, LIFTER_HOST, PORT_LIFTER) < 0)
    {
        printf("Could not connect [hal: IO-Bricklet]\n");
        status = 1;
    }
    else
    {
        status = 0;
    }

    io16_set_edge_count_config(&io, 0, IO16_EDGE_TYPE_BOTH, 0);
#endif

    return status;
}

/**
 * - disconnects the motor by destroying the "entities" of the IO-Bricklet and 
 * the IPConnection
 */
extern void hal_shutdown(void)
{
#ifdef HW
    io16_destroy(&io);
    ipcon_destroy(&ipcon);
#endif
}

/**
 * - reads the hall-counts, since the last call of his function
 */
extern int hal_get_hall_count(void)
{

#ifdef HW
    uint32_t hall_count = 0;
    io16_get_edge_count(&io, 0, true, &hall_count);
#endif

#ifdef SW
    int hall_count = 0;
    
    if (!bas_get_state() && !tas_get_state())
    {
        wrp_get_data(WRP_HAL_COUNT, &hall_count);
    }
    else
    {

    }
#endif

    return (int) hall_count;
}