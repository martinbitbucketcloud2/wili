#ifndef MDC_H
#define MDC_H

/**
 * - direction-commands for the MDC
 */
typedef enum MdcDir_ {
    MDC_DIR_UP,
    MDC_DIR_DOWN,
    MDC_DIR_STOP
} MdcDir;

/**
 * - speed-commands for the MDC
 */
typedef enum MdcSpeed_ {
    MDC_SPEED_LOW,
    MDC_SPEED_MIDDLE,
    MDC_SPEED_HIGH
} MdcSpeed;

extern int mdc_init(void);
extern void mdc_shutdown(void);

extern void mdc_on(void);
extern void mdc_off(void);

extern void mdc_do_cmd(MdcDir dir);
extern void mdc_set_speed(MdcSpeed speed);
extern int mdc_get_consumption(void);



#endif /* MDC_H */