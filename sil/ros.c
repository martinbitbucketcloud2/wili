/* 
 * - rocker-switch
 */

#include <stdio.h>
#include <stdlib.h>

#include "../tinkerforge/bricklet_io4.h"
#include "../tinkerforge/ip_connection.h"

#include "ros.h"

#include "constants.h"
#include "wrp.h"

#ifdef HW
/**
 * - defines an "entity" of the IO-4-Bricklet and an IPConnection
 */
static IO4 io;
static IPConnection ipcon;
#endif

/**
 * - initializes the Rocker Switch by connecting to the IO-4-Bricklet
 */
extern int ros_init(void)
{
    int status = 0;

#ifdef HW
    ipcon_create(&ipcon);

    io4_create(&io, IO4_UID, &ipcon);

    if (ipcon_connect(&ipcon, HANDHELD_HOST, PORT_LIFTER) < 0)
    {
        printf("Could not connect [ros: IO4-Bricklet]\n");
        status = 1;
    }
    else
    {
        status = 0;
    }
#endif

    return status;
}

/**
 * - disconnects the bottom rocker switch by destroying the "entities" of 
 * the IO-4-Bricklet and the IPConnection
 */
extern void ros_shutdown(void)
{
#ifdef HW
    io4_destroy(&io);
    ipcon_destroy(&ipcon);
#endif
}

/**
 * - checks and returns, whether the rocker switch is up, down or not pressed
 */
extern int ros_get_position(void)
{
    int ros_state = 0;

#ifdef HW
    uint8_t value_mask_ros = 0x00;

    if (io4_get_value(&io, &value_mask_ros) < 0)
    {
        printf("Could not get value as bitmask, probably timeout"
            "[ros: IO-4-Bricklet]\n");
    }
    else
    {

    }

    if (0x03 == (value_mask_ros & 0x03))
    {
        ros_state = 0;
    }
    else if (0x01 == (value_mask_ros & 0x03))
    {
        ros_state = -1;
    }
    else if (0x02 == (value_mask_ros & 0x03))
    {
        ros_state = 1;
    }
    else
    {
        ros_state = 0;
    }
#endif

#ifdef SW
    wrp_get_data(WRP_ROS_STATE, &ros_state);
#endif

    return ros_state;
}