#ifndef ROS_H
#define ROS_H

extern int ros_init(void);
extern void ros_shutdown(void);

extern int ros_get_position(void);

#endif /* ROS_H */