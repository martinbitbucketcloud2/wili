#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#ifdef _WIN32
/* Headerfiles für Windows */
#include <winsock.h>
#include <io.h>
#else
/* Headerfiles für UNIX/Linux */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#endif

#include "constants.h"

#include "wrp.h"


struct sockaddr_in addr_server;

#ifdef _WIN32
SOCKET sock;
#else
int sock;
#endif

static const char del[] = "_";
char send_msg_copy[MSG_LEN] = {0};
char recv_msg[MSG_LEN] = {0};
char *send_msg_id, *send_msg_type;
char *recv_msg_id, *recv_msg_type, *recv_msg_val;


static int create_client(void);
static int connect_to_socket(void);
static void close_socket(void);

static int send_and_receive(char *msg, void *val);

static int is_msg_part_valid(char *msg_id, char *recv_msg);

static int intrpr_get_data(char *send_msg_id, char *send_msg_type,
    char *recv_msg_id, char *recv_msg_type, char *recv_msg_val,
    void *ptr_to_val);

static int check_set_data(char *send_msg_id, char *send_msg_type,
    char *recv_msg_id, char *recv_msg_type);

static int get_bas_state(char *recv_msg_val, void *ptr_to_val);
static int get_tas_state(char *recv_msg_val, void *ptr_to_val);
static int get_kos_state(char *recv_msg_val, void *ptr_to_val);
static int get_ros_state(char *recv_msg_val, void *ptr_to_val);
static int get_hal_state(char *recv_msg_val, void *ptr_to_val);
static int get_nvm_value(char *recv_msg_val, void *ptr_to_val);
static int get_mdc_current(char *recv_msg_val, void *ptr_to_val);
static int get_mst_current(char *recv_msg_val, void *ptr_to_val);
static int get_mst_steps(char *recv_msg_val, void *ptr_to_val);

static void print_err_code(char *err_msg);
static void print_red(char *msg, long val);
static void print_green(char *msg);

/**
 * - initializes the Wrapper by creating a client and connecting to the socket
 */
extern int wrp_init(void)
{
    int status = 0;

#ifdef SW
    if (create_client() || connect_to_socket())
    {
        status = 1;
    }
    else
    {

    }
#endif

    return status;
}

/**
 * - closes the connection to the socket
 */
extern void wrp_shutdown(void)
{
#ifdef SW
    close_socket();
#endif
}

extern int wrp_get_data(char *msg, void *ptr_to_val)
{
    int status = 0;

    if (send_and_receive(msg, ptr_to_val))
    {
        status = 1;
    }
    else
    {

    }

    return status;
}

extern int wrp_set_data(char *send_msg)
{
    int status = 0;

    if (send_and_receive(send_msg, NULL))
    {
        status = 1;
    }
    else
    {

    }

    return status;
}

static int create_client(void)
{
    int status = 0;
    unsigned long addr;
    struct hostent *host_info;

#ifdef _WIN32
    WORD wVersionRequested;
    WSADATA wsaData;
    wVersionRequested = MAKEWORD(1, 1);
    if (WSAStartup(wVersionRequested, &wsaData) != 0)
    {
        print_err_code("Winsock creation failed, try again.\n");
        status = 1;
    }
    else
    {

    }
#endif

    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0)
    {
        print_err_code("Socket creation failed, try again.");
        status = 1;
    }
    else
    {

    }

    if (0 == status)
    {
        memset(&addr_server, 0, sizeof (addr_server));

        if ((addr = inet_addr(IP_ADDR)) != INADDR_NONE)
        {
            memcpy((char *) &addr_server.sin_addr, &addr, sizeof (addr));
        }
        else
        {
            if (NULL == (host_info = gethostbyname(IP_ADDR)))
            {
                print_err_code("Server unknown, try again.");
                status = 1;
            }
            else //if(!strcmp(IP_ADDR, "localhost"))
            {
                memcpy((char *) &addr_server.sin_addr,
                    host_info->h_addr, host_info->h_length);
            }
        }

        addr_server.sin_family = AF_INET;
        addr_server.sin_port = htons(PORT_UI);
    }
    else
    {

    }

    return status;
}

static void close_socket(void)
{

#ifdef _WIN32
    closesocket(sock);
    WSACleanup();
#else
    close(sock);
#endif

    printf("Socket closed!\n");
}

static int connect_to_socket(void)
{
    int status = 0;

    if (connect(sock, (struct sockaddr*) &addr_server,
        sizeof (addr_server)) < 0)
    {
        print_err_code("No connection to server, try again");
        status = 1;
    }
    else
    {
        print_green("Server connection successful!");
    }

    return status;
}

static int send_and_receive(char *send_msg, void *ptr_to_val)
{
    int status = 0;
    int msg_len = strlen(send_msg);

    memset(recv_msg, '\0', MSG_LEN);
    memset(send_msg_copy, '\0', MSG_LEN);
    strcpy(send_msg_copy, send_msg);

    send_msg_id = strtok(send_msg_copy, del);
    send_msg_type = strtok(NULL, del);


    if (send(sock, send_msg, msg_len, 0) != msg_len)
    {
        print_err_code("Message was not send completly");
        status = 1;
    }
    else
    {

    }


    if (recv(sock, recv_msg, sizeof (recv_msg), 0) < 0)
    {
        print_err_code("Message was not received completly");
        status = 1;
    }
    else
    {
        recv_msg_id = strtok(recv_msg, del);
        recv_msg_type = strtok(NULL, del);
        recv_msg_val = strtok(NULL, del);

        if (ptr_to_val != NULL)
        {
            intrpr_get_data(send_msg_id, send_msg_type, recv_msg_id,
                recv_msg_type, recv_msg_val, ptr_to_val);
        }
        else
        {
            check_set_data(send_msg_id, send_msg_type, recv_msg_id,
                recv_msg_type);
        }
    }

    return status;
}

static int is_msg_part_valid(char *send_msg_part, char *recv_msg_part)
{
    int validation = 0;

    validation = !(strcmp(send_msg_part, recv_msg_part));

    if (validation)
    {
        /*
        printf(GREEN "Expected: %s, Received: %s" RESET "\n",
            send_msg_part, recv_msg_part); 
        /**/
    }
    else
    {
        printf(RED "Expected: %s, Received: %s" RESET "\n",
            send_msg_part, recv_msg_part);
    }
    return validation;
}

static int intrpr_get_data(char *send_msg_id, char *send_msg_type,
    char *recv_msg_id, char *recv_msg_type, char *recv_msg_val,
    void *ptr_to_val)
{
    int status = 0;

    if (is_msg_part_valid(send_msg_id, recv_msg_id) &&
        is_msg_part_valid(send_msg_type, recv_msg_type))
    {
        switch (recv_msg_id[0])
        {
            case 'B':
                get_bas_state(recv_msg_val, ptr_to_val);
                break;

            case 'H':
                get_hal_state(recv_msg_val, ptr_to_val);
                break;

            case 'K':
                get_kos_state(recv_msg_val, ptr_to_val);
                break;

            case 'M':
                if ('D' == recv_msg_id[1])
                {
                    get_mdc_current(recv_msg_val, ptr_to_val);
                }
                else if ('S' == recv_msg_id[1] && 'C' == recv_msg_type[0])
                {
                    get_mst_current(recv_msg_val, ptr_to_val);
                }
                else if ('S' == recv_msg_id[1] && 'S' == recv_msg_type[0])
                {
                    get_mst_steps(recv_msg_val, ptr_to_val);
                }
                else
                {
                    printf(RED "Received message does not contain any correct"
                        " data: %s" RESET, recv_msg_id);
                    status = 1;
                }
                break;

            case 'N':
                get_nvm_value(recv_msg_val, ptr_to_val);
                break;

            case 'R':
                get_ros_state(recv_msg_val, ptr_to_val);
                break;

            case 'T':
                get_tas_state(recv_msg_val, ptr_to_val);
                break;

            default:
                printf(RED "Received message does not contain any correct"
                    " data: %s" RESET, recv_msg_id);
                status = 1;
                break;
        }
    }
    else
    {
        status = 1;
    }

    return status;
}

static int get_bas_state(char *recv_msg_val, void *ptr_to_val)
{
    int status = 0;
    long msg_val;

    msg_val = strtol(recv_msg_val, NULL, 0);

    if (1 != msg_val && 0 != msg_val)
    {
        print_red("bas: Data received does not contain a valid value", msg_val);
        status = 1;
    }
    else
    {
        * (int *) ptr_to_val = msg_val;
    }

    return status;
}

static int get_tas_state(char *recv_msg_val, void *ptr_to_val)
{
    int status = 0;
    long msg_val;

    msg_val = strtol(recv_msg_val, NULL, 0);

    if (1 != msg_val && 0 != msg_val)
    {
        print_red("tas: Data received does not contain a valid value", msg_val);

        status = 1;
    }
    else
    {
        * (int *) ptr_to_val = msg_val;
    }

    return status;
}

static int get_kos_state(char *recv_msg_val, void *ptr_to_val)
{
    int status = 0;
    long msg_val;

    msg_val = strtol(recv_msg_val, NULL, 0);

    if (1 != msg_val && 0 != msg_val)
    {
        print_red("kos: Data received does not contain a valid value", msg_val);

        status = 1;
    }
    else
    {
        * (int *) ptr_to_val = msg_val;
    }

    return status;
}

static int get_ros_state(char *recv_msg_val, void *ptr_to_val)
{
    int status = 0;
    long msg_val;

    msg_val = strtol(recv_msg_val, NULL, 0);

    if ((-1 != msg_val && 0 != msg_val && 1 != msg_val))
    {
        print_red("ros: Data received does not contain a valid value", msg_val);

        status = 1;
    }
    else
    {
        * (int *) ptr_to_val = msg_val;
    }

    return status;
}

static int get_hal_state(char *recv_msg_val, void *ptr_to_val)
{
    int status = 0;
    long msg_val;

    msg_val = strtol(recv_msg_val, NULL, 0);

    if (0 > msg_val)
    {
        print_red("hal: Data received does not contain a valid value", msg_val);

        status = 1;
    }
    else
    {
        * (int *) ptr_to_val = msg_val;
    }

    return status;
}

static int get_nvm_value(char *recv_msg_val, void *ptr_to_val)
{
    int status = 0;
    long msg_val;

    msg_val = strtol(recv_msg_val, NULL, 0);

    if (-999 > msg_val || 9999 < msg_val)
    {
        print_red("nvm: Data received does not contain a valid value", msg_val);

        status = 1;
    }
    else
    {
        * (int *) ptr_to_val = msg_val;
    }

    return status;
}

static int get_mdc_current(char *recv_msg_val, void *ptr_to_val)
{
    int status = 0;
    long msg_val;

    msg_val = strtol(recv_msg_val, NULL, 0);

    if (0 > msg_val)
    {
        print_red("mdc: Data received does not contain a valid value", msg_val);

        status = 1;
    }
    else
    {
        * (int *) ptr_to_val = msg_val;
    }

    return status;
}

static int get_mst_current(char *recv_msg_val, void *ptr_to_val)
{
    int status = 0;
    long int msg_val;

    msg_val = strtol(recv_msg_val, NULL, 0);

    if (0 > msg_val)
    {
        print_red("mst: Data received does not contain a valid value", msg_val);

        status = 1;
    }
    else
    {
        * (int *) ptr_to_val = msg_val;
    }

    return status;
}

static int get_mst_steps(char *recv_msg_val, void *ptr_to_val)
{
    int status = 0;
    long msg_val;

    msg_val = strtol(recv_msg_val, NULL, 0);

    if (0 > msg_val)
    {
        print_red("mst: Data received does not contain a valid value", msg_val);

        status = 1;
    }
    else
    {
        * (int *) ptr_to_val = msg_val;
    }

    return status;
}

static int check_set_data(char *send_msg_id, char *send_msg_type,
    char *recv_msg_id, char *recv_msg_type)
{
    int status = 0;

    if (!is_msg_part_valid(send_msg_id, recv_msg_id) ||
        !is_msg_part_valid(send_msg_type, recv_msg_type))
    {
        status = 1;
    }
    else
    {

    }

    return status;
}

static void print_err_code(char *err_msg)
{
#ifdef _WIN32
    fprintf(stderr, "%s: %d\n", err_msg, WSAGetLastError());
#else
    fprintf(stderr, RED "%s: %s" RESET, err_msg, strerror(errno));
#endif
}

static void print_red(char *msg, long val)
{
    printf(RED "%s: %li" RESET, msg, val);
}

static void print_green(char *msg)
{
    printf(GREEN "%s" RESET, msg);
}