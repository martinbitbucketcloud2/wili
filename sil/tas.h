#ifndef TAS_H
#define TAS_H

extern int tas_init(void);
extern void tas_shutdown(void);

extern int tas_get_state(void);

#endif /* TAS_H */

