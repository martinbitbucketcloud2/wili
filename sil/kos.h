#ifndef KOS_H
#define KOS_H

extern int kos_init(void);
extern void kos_shutdown(void);

extern int kos_get_signal(void);

#endif /* KOS_H */