/* 
 * - stepper-motor
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../tinkerforge/brick_stepper.h"
#include "../tinkerforge/ip_connection.h"

#include "mst.h"

#include "constants.h"
#include "wrp.h"

#ifdef HW
/**
 * - defines an "entity" of the Stepper-Brick
 */
static Stepper mst;

/**
 * - defines an "entity" of the IPConnection
 */
IPConnection ipcon_mst;
#endif

/**
 * - initializes the Stepper by connecting to the Stepper-Brick
 *  - sets the maximum motor current, the step mode, the maximum velocity, the 
 * speed ramping, the synchronous rectification, the decay and resets the 
 * current position
 */
extern int mst_init(void)
{
    int status = 0;

#ifdef HW
    ipcon_create(&ipcon_mst);

    stepper_create(&mst, MST_UID, &ipcon_mst);

    if (ipcon_connect(&ipcon_mst, LIFTER_HOST, PORT_LIFTER) < 0)
    {
        printf("Could not connect [stp: Stepper-Brick]\n");
        status = 1;
    }
    else
    {
        status = 0;
    }

    stepper_set_motor_current(&mst, 800);
    stepper_set_step_mode(&mst, 8);

    stepper_set_max_velocity(&mst, 1800);
    stepper_set_speed_ramping(&mst, 65535, 65535);

    stepper_set_sync_rect(&mst, 1);
    stepper_set_decay(&mst, 65535);

    stepper_set_current_position(&mst, 0);
#endif

    return status;
}

/**
 * - disconnects the motor by destroying the "entities" of the Stepper-Brick and 
 * the IPConnection
 */
extern void mst_shutdown(void)
{
    mst_off();

#ifdef HW    
    stepper_destroy(&mst);
    ipcon_destroy(&ipcon_mst);
#endif
}

/**
 * - enables the motor
 */
extern void mst_on(void)
{
#ifdef HW
    stepper_enable(&mst);
#endif

#ifdef SW
    wrp_set_data(WRP_MST_ENABLE_ON);
#endif
}

/**
 * - disables the motor
 */
extern void mst_off(void)
{
#ifdef HW
    stepper_disable(&mst);
#endif

#ifdef SW
    wrp_set_data(WRP_MST_ENABLE_OFF);
#endif
}

/**
 * - sets the direction of the stepper-motor
 */
extern void mst_do_cmd(MstDir dir)
{
    switch (dir)
    {
        case MST_DIR_STOP:
#ifdef HW
            stepper_stop(&mst);
#endif

#ifdef SW
            wrp_set_data(WRP_MST_DIR_STOP);
#endif
            break;

        case MST_DIR_UP:
#ifdef HW
            stepper_drive_forward(&mst);
#endif

#ifdef SW
            wrp_set_data(WRP_MST_DIR_UP);
#endif
            break;

        case MST_DIR_DOWN:
#ifdef HW
            stepper_drive_backward(&mst);
#endif

#ifdef SW
            wrp_set_data(WRP_MST_DIR_DOWN);
#endif
            break;

        default:
#ifdef HW
            stepper_stop(&mst);
#endif

#ifdef SW
            wrp_set_data(WRP_MST_DIR_STOP);
#endif
            break;
    }
}

/*
 * - specifies a direction and the nummber of steps, which the stepper-motor 
 * should drive
 */
extern void mst_do_steps(MstDir dir, int32_t steps)
{
#ifdef SW
    char msg[17];
#endif

    if (0 > steps || 9999 < steps)
    {
        printf("Only steps greater than 0 und lesser than 9999\n");

#ifdef SW
        strcpy(msg, WRP_MST_DIR_STOP);
#endif

    }
    else
    {
        switch (dir)
        {
            case MST_DIR_UP:
#ifdef HW
                stepper_set_steps(&mst, steps);
#endif
                
#ifdef SW
                sprintf(msg, WRP_MST_DOSTEPS, steps);
#endif

                break;

            case MST_DIR_DOWN:
                steps *= (-1);
#ifdef HW
                stepper_set_steps(&mst, steps);
#endif
                
#ifdef SW

                sprintf(msg, WRP_MST_DOSTEPS, steps);
#endif

                break;

            case MST_DIR_STOP:
            default:
#ifdef HW
                stepper_set_steps(&mst, 0);
#endif
                
#ifdef SW

                strcpy(msg, WRP_MST_DIR_STOP);
#endif

                break;
        }
    }

#ifdef SW
    wrp_set_data(msg);
#endif
}

/*
 * - sets the speed of the stepper-motor depending on the commited value
 */
extern void mst_set_speed(MstSpeed spe)
{
#ifdef HW
    uint16_t stepper_speed;
#endif

    switch (spe)
    {
        case MST_SPEED_LOW:
#ifdef HW
            stepper_speed = 900;
#endif

#ifdef SW
            wrp_set_data(WRP_MST_SPEED_LOW);
#endif
            break;

        case MST_SPEED_MID:
#ifdef HW
            stepper_speed = 1350;
#endif

#ifdef SW
            wrp_set_data(WRP_MST_SPEED_MIDDLE);
#endif
            break;

        case MST_SPEED_HIGH:
#ifdef HW
            stepper_speed = 1800;
#endif


#ifdef SW
            wrp_set_data(WRP_MST_SPEED_HIGH);
#endif      
            break;

        default:
#ifdef HW

            stepper_speed = 0;
#endif
            break;
    }

#ifdef HW
    stepper_set_max_velocity(&mst, stepper_speed);
#endif
}

/**
 * - reads the current consumption of the stepper-motor
 */
extern int mst_get_consumption(void)
{
#ifdef HW
    uint16_t current_consumption = 0;

    stepper_get_current_consumption(&mst, &current_consumption);
#endif

#ifdef SW
    int current_consumption = 0;

    wrp_get_data(WRP_MST_CURRENT, &current_consumption);
#endif
    return (int) current_consumption;
}

/**
 * - reads the steps, which the stepper-motor has made, since the last call of 
 * this function 
 */
extern int mst_get_steps(void)
{
#ifdef HW
    int32_t current_steps = 0;

    stepper_get_current_position(&mst, &current_steps);
    stepper_set_current_position(&mst, 0);
#endif

#ifdef SW
    int current_steps = 0;
    wrp_get_data(WRP_MST_STEPS, &current_steps);
#endif

    if (0 > current_steps)
    {
        current_steps *= (-1);
    }
    else
    {

    }

    return (int) current_steps;
}