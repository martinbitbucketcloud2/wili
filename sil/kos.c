/* 
 * - key-switch
 */

#include <stdio.h>
#include <stdlib.h>

#include "../tinkerforge/bricklet_io4.h"
#include "../tinkerforge/ip_connection.h"

#include "kos.h"

#include "constants.h"
#include "wrp.h"

#ifdef HW
/**
 * - defines an "entity" of the IO-4-Bricklet and an IPConnection
 */
static IO4 io;
static IPConnection ipcon;
#endif

/**
 * - initializes the key switch by connecting to the IO-4-Bricklet
 */
extern int kos_init(void)
{
    int status = 0;

#ifdef HW
    ipcon_create(&ipcon);

    io4_create(&io, IO4_UID, &ipcon);

    if (ipcon_connect(&ipcon, HANDHELD_HOST, PORT_LIFTER) < 0)
    {
        printf("Could not connect [kos: IO-4-Bricklet]\n");
        status = 1;
    }
    else
    {
        status = 0;
    }
#endif

    return status;
}

/**
 * - disconnects the key switch by destroying the "entities" of 
 * the IO-4-Bricklet and the IPConnection
 */
extern void kos_shutdown(void)
{
#ifdef HW
    io4_destroy(&io);
    ipcon_destroy(&ipcon);
#endif
}

/**
 * - checks and returns, whether the key switch is on or off
 */
extern int kos_get_signal(void)
{
    int kos_signal = 0;

#ifdef HW
    uint8_t value_mask_ks = 0x00;

    if (io4_get_value(&io, &value_mask_ks) < 0)
    {
        printf("Could not get value as bitmask, probably timeout"
            "[kos: IO-4-Bricklet]\n");
    }
    else
    {

    }

    if (0x00 == (value_mask_ks & 0x04))
    {
        kos_signal = 1;
    }
    else if (0x04 == (value_mask_ks & 0x04))
    {
        kos_signal = 0;
    }
    else
    {

    }
#endif

#ifdef SW
    wrp_get_data(WRP_KOS_STATE, &kos_signal);
#endif

    return kos_signal;
}