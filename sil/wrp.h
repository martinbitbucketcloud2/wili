#ifndef WRP_H
#define WRP_H

#define WRP_BAS_STATE "BAS_STATE"
#define WRP_HAL_COUNT "HAL_COUNT"
#define WRP_KOS_STATE "KOS_STATE"
#define WRP_ROS_STATE "ROS_STATE"
#define WRP_TAS_STATE "TAS_STATE"

#define WRP_NVM_READ "NVM_READ"
#define WRP_NVM_WRITE "NVM_WRITE_%i"

#define WRP_MDC_ENABLE_ON "MDC_ENABLE_ON"
#define WRP_MDC_ENABLE_OFF "MDC_ENABLE_OFF"
#define WRP_MDC_SPEED_LOW "MDC_SPEED_LOW"
#define WRP_MDC_SPEED_MIDDLE "MDC_SPEED_MIDDLE"
#define WRP_MDC_SPEED_HIGH "MDC_SPEED_HIGH"
#define WRP_MDC_DIR_STOP "MDC_DIR_STOP"
#define WRP_MDC_DIR_UP "MDC_DIR_UP"
#define WRP_MDC_DIR_DOWN "MDC_DIR_DOWN"
#define WRP_MDC_CURRENT "MDC_CURRENT"

#define WRP_MST_ENABLE_ON "MST_ENABLE_ON"
#define WRP_MST_ENABLE_OFF "MST_ENABLE_OFF"
#define WRP_MST_SPEED_LOW "MST_SPEED_LOW"
#define WRP_MST_SPEED_MIDDLE "MST_SPEED_MIDDLE"
#define WRP_MST_SPEED_HIGH "MST_SPEED_HIGH"
#define WRP_MST_DIR_STOP "MST_DIR_STOP"
#define WRP_MST_DIR_UP "MST_DIR_UP"
#define WRP_MST_DIR_DOWN "MST_DIR_DOWN"
#define WRP_MST_DOSTEPS "MST_DOSTEPS_%i"
#define WRP_MST_STEPS "MST_STEPS"
#define WRP_MST_CURRENT "MST_CURRENT"

#define WRP_DIS_ENABLE_OFF "DIS_ENABLE_OFF"
#define WRP_DIS_SHOW "DIS_SHOW_%04d"

#define GREEN "\x1b[32m"
#define RED "\x1b[31m"
#define RESET "\x1b[0m \n"

extern int wrp_init(void);
extern void wrp_shutdown(void);

extern int wrp_get_data(char *send_msg, void *ptr_to_val);
extern int wrp_set_data(char *send_msg);

#endif /* WRP_H */

