/* 
 * -non-volatile-memory
 */

#include <string.h>
#include <stdio.h>

#include "nvm.h"

#include "constants.h"
#include "wrp.h"

/**
 * - pointer to a file, in which the window position should be saved or read
 */
FILE* memory;

/**
 * - opens the file
 * - writes the value of the commited variable in the file
 * - closes the file
 */
extern void nvm_write_value(int value)
{
    if (-999 > value || 9999 < value)
    {
        printf("nvm: Only values greater than -1000 und lesser than 10.000\n");
    }
    else
    {
#ifdef HW   
        memory = fopen(FILENAME, "w+");

        if (NULL == memory)
        {
            printf("fopen() failed!\n");
            return;
        }
        else
        {

        }

        fprintf(memory, "%i", value);

        fclose(memory);
#endif

#ifdef SW
        char msg[16];
        sprintf(msg, WRP_NVM_WRITE, value);
        wrp_set_data(msg);
#endif
    }
}

/**
 * - opens the file
 * - reads the file
 * - closes the file
 * - returns the value, which was written in the file
 */
extern int nvm_read_value(void)
{
    int value = 0;

#ifdef HW
    memory = fopen(FILENAME, "rw+");

    if (NULL == memory)
    {
        printf("fopen() failed!\n");
        return 0;
    }
    else
    {

    }

    fscanf(memory, "%i", &value);

    fclose(memory);
#endif

#ifdef SW
    wrp_get_data(WRP_NVM_READ, &value);
#endif

    return value;
}