/* 
 * - seven-segement-display
 */

#include <stdio.h>

#include "../tinkerforge/bricklet_segment_display_4x7.h"
#include "../tinkerforge/ip_connection.h"

#include "dis.h"

#include "constants.h"
#include "wrp.h"

#ifdef HW
/**
 * - defines an "entity" of the DC-Brick and an IPConnection
 */
static SegmentDisplay4x7 sd;

/**
 * - defines an "entity" of the IPConnection
 */
static IPConnection ipcon;
#endif

/**
 * - array with bitmasks, whichs represents the numbers
 */
const uint8_t digits[] = {0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x07,
    0x7f, 0x6f, 0x40}; // 0~9,-

/**
 * - initializes the Segment by connecting to the Segment-Display-Bricklet
 */
extern int dis_init(void)
{
    int status = 0;

#ifdef HW
    ipcon_create(&ipcon);

    segment_display_4x7_create(&sd, SEG_UID, &ipcon);

    if (ipcon_connect(&ipcon, HANDHELD_HOST, PORT_LIFTER) < 0)
    {
        printf("Could not connect [dis: IO-Bricklet]\n");
        status = 1;
    }
    else
    {
        status = 0;
    }

    segment_display_4x7_set_segments(&sd, ((uint8_t [4])
    {
        0
    }), 7, false);
#endif

    dis_turn_off();

    return status;
}

/**
 * - disconnects the Segment-Display by destroying the "entities" of 
 * the Segment and the IPConnection
 */
extern void dis_shutdown(void)
{
    dis_turn_off();

#ifdef HW
    segment_display_4x7_destroy(&sd);
    ipcon_destroy(&ipcon);
#endif
}

/**
 * - shows the commited value on the Segment-Display
 */
extern void dis_show_value(int value)
{
    if (-999 > value || 9999 < value)
    {
        //printf("Only values greater than -1000 und lesser than 10.000\n");
        value = 0;
    }
    else
    {
#ifdef HW
        uint8_t segments[4];
        int number_to_display = value;

        if (0 <= number_to_display)
        {
            int i;
            for (i = 0; i < 4; i++)
            {
                segments[3 - i] = digits[number_to_display % 10];
                number_to_display = number_to_display / 10;
            }
        }
        else
        {
            number_to_display = number_to_display * (-1);

            int i;
            for (i = 0; i < 3; i++)
            {
                segments[3 - i] = digits[number_to_display % 10];
                number_to_display = number_to_display / 10;
            }

            segments[0] = 0x40;
        }

        segment_display_4x7_set_segments(&sd, segments, 7, false);
#endif

#ifdef SW
        char send_msg[14];
        sprintf(send_msg, WRP_DIS_SHOW, value);
        wrp_set_data(send_msg);
#endif
    }
}

/**
 * - turns the display of the Segment off
 */
extern void dis_turn_off(void)
{
#ifdef HW

    segment_display_4x7_set_segments(&sd, ((uint8_t [4])
    {
        0
    }), 7, false);
#endif

#ifdef SW
    wrp_set_data(WRP_DIS_ENABLE_OFF);
#endif
}