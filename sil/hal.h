#ifndef HAL_H
#define HAL_H

extern int hal_init(void);
extern void hal_shutdown(void);

extern int hal_get_hall_count(void);

#endif /* HAL_H */

