/* 
 * - bottom-snap-action-switch
 */
#include <stdio.h>


#include "../tinkerforge/ip_connection.h"
#include "../tinkerforge/bricklet_io16.h"

#include "bas.h"

#include "constants.h"
#include "wrp.h"

#ifdef HW
/**
 * - defines an "entity" of the IO-16-Bricklet and an IPConnection
 */
static IO16 io;
static IPConnection ipcon;
#endif

/**
 * - initializes the Bottom Sensor by connecting to the IO-16-Bricklet
 */
extern int bas_init(void)
{
    int status = 0;

#ifdef HW
    ipcon_create(&ipcon);

    io16_create(&io, IO16_UID, &ipcon);

    if (ipcon_connect(&ipcon, LIFTER_HOST, PORT_LIFTER) < 0)
    {
        printf("Could not connect [bas: IO-Bricklet]\n");
        status = 1;
    }
    else
    {
        status = 0;
    }
#endif

    return status;
}

/**
 * - disconnects the bottom snap-action switch by destroying the "entities" of 
 * the IO-16-Bricklet and the IPConnection
 */
extern void bas_shutdown(void)
{
#ifdef HW
    io16_destroy(&io);
    ipcon_destroy(&ipcon);
#endif
}

/**
 * - checks and returns, whether the bottom snap-action switch is pressed or not
 */
extern int bas_get_state(void)
{
    int bas_state = 0;


#ifdef HW
    uint8_t value_mask_bps = 0x00;

    if (io16_get_port(&io, 'a', &value_mask_bps) < 0)
    {
        printf("Could not get value from port A as bitmask, probably timeout"
            "[bs: IO-Bricklet]\n");
    }
    else
    {

    }

    if (0x00 == (value_mask_bps & 0x08))
    {
        bas_state = 1;
    }
    else
    {
        bas_state = 0;
    }
#endif

#ifdef SW
    wrp_get_data(WRP_BAS_STATE, &bas_state);
#endif

    return bas_state;
}