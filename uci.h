#ifndef UCI_H
#define UCI_H

/**
 * - user-request of the User Command Interpreter
 */
typedef enum UciRequest_
{
    UCI_UP,
    UCI_DOWN,
    UCI_STOP
} UciRequest;

extern void uci_init(void);
extern void uci_shutdown(void);

extern UciRequest uci_get_user_request(void);
extern void uci_tell_window_stopped(void);

#endif /* UCI_H */