#include "uci.h"


/**
 * - states of the physical rocker
 */
typedef enum RockerState_
{
    ROCKER_UP = 1,
    ROCKER_STOP = 0,
    ROCKER_DOWN = -1
} RockerState;

/**
 * - states of the User Command Interpreter
 */
typedef enum UciState_
{
    STOP,
    STOPPED,
    UP_STARTED,
    SIMPLE_UP,
    COMFORT_UP,
    DOWN_STARTED,
    SIMPLE_DOWN,
    COMFORT_DOWN
} UciState;

/**
 * - variable to save the current state of the User Command Interpreter
 */
static UciState uci_state;

/**
 * - the time in ms, in which the rocker have to be pressed and released
 * to enable comfort mode
 */
const unsigned int TIME_FOR_COMFORT_MODE = 500;

/**
 * - prototypes of used functions
 */
static RockerState get_rocker(void);
static UciState get_next_state(UciState state, RockerState rocker);

static UciState in_stop(RockerState rocker);
static UciState in_stopped(RockerState rocker);
static UciState in_up_started(RockerState rocker);
static UciState in_simple_up(RockerState rocker);
static UciState in_comfort_up(RockerState rocker);
static UciState in_down_started(RockerState rocker);
static UciState in_simple_down(RockerState rocker);
static UciState in_comfort_down(RockerState rocker);

/**
 * - initializes the User Command Interpreter 
 *  - calls the init-function of the physical rocker
 *  - initializes the two time-structs
 *  - sets the initial state of the User Command Interpreter to STOP
 */
extern void uci_init(void)
{
    ros_init();

    uci_state = STOP;
}

/**
 * - shuts down the User Command Interpreter by calling the shutdown-function of
 * the rocker
 */
extern void uci_shutdown(void)
{
    ros_shutdown();
}

/**
 * - get the request of the user
 *  - switch to next state depending on current state and the positon of the
 * physical rocker
 */
extern UciRequest uci_get_user_request(void)
{
    static UciRequest request = UCI_STOP;

    RockerState rocker = get_rocker();

    uci_state = get_next_state(uci_state, rocker);

    switch (uci_state)
    {
        case STOPPED:
            request = UCI_STOP;
            break;
        case UP_STARTED:
        case SIMPLE_UP:
        case COMFORT_UP:
            request = UCI_UP;
            break;
        case DOWN_STARTED:
        case SIMPLE_DOWN:
        case COMFORT_DOWN:
            request = UCI_DOWN;
            break;
        case STOP:
            request = UCI_STOP;
            break;
        default:
            request = UCI_STOP;
#ifdef DEBUG
            printf("uci:\t uci_get_user_request: Failure!\n");
#endif     
            break;
    }

    return request;
}

/**
 * - sets the uci_state to STOPPED
 */
extern void uci_tell_window_stopped(void)
{

}



/**
 * - checks and returns, in which position the physical rocker is
 */
static RockerState get_rocker(void)
{
    RockerState rocker_state = ROCKER_STOP;
    
    int ros_position = 0;
    
    ros_position = ros_get_position();


    if (0 == ros_position)
    {
        rocker_state = ROCKER_STOP;
    }
    else if (-1 == ros_position)
    {
        rocker_state = ROCKER_DOWN;
    }
    else if (1 == ros_position)
    {
        rocker_state = ROCKER_UP;
    }
    else
    {
        rocker_state = ROCKER_STOP;
    }

    return rocker_state;
}

/**
 * - gets the time difference, in which the rocker is pressed and released 
 */
static int get_rocker_time_difference(void)
{
    
}

/**
 * - behaviour of the User Command Interpreter implemented as a state machine
 *  - switch to next state depending on current state and the positon of the
 * physical rocker
 */
static UciState get_next_state(UciState state, RockerState rocker)
{
    UciState next_state = STOPPED;

    switch (state)
    {
        case STOP:
            next_state = in_stop(rocker);
            break;
        case STOPPED:
            next_state = in_stopped(rocker);
            break;
        case UP_STARTED:
            next_state = in_up_started(rocker);
            break;
        case SIMPLE_UP:
            next_state = in_simple_up(rocker);
            break;
        case COMFORT_UP:
            next_state = in_comfort_up(rocker);
            break;
        case DOWN_STARTED:
            next_state = in_down_started(rocker);
            break;
        case SIMPLE_DOWN:
            next_state = in_simple_down(rocker);
            break;
        case COMFORT_DOWN:
            next_state = in_comfort_down(rocker);
            break;
        default:
            next_state = STOPPED;
#ifdef DEBUG
            printf("uci:\t get_next_state: Failure!\n");
#endif
            break;
    }

    return next_state;
}

/**
 * - behaviour of the in_stop-state depending on the position of the 
 * physical rocker implemented as a state machine
 * - sets the start-time
 */
static UciState in_stop(RockerState rocker)
{
    UciState state = STOPPED;
    switch (rocker)
    {
        case ROCKER_STOP:
            state = STOP;
            break;
        case ROCKER_UP:
            state = UP_STARTED;
            break;
        case ROCKER_DOWN:
            state = DOWN_STARTED;
            break;
        default:
            state = STOP;
#ifdef DEBUG
            printf("uci:\t in_stop: Failure!\n");
#endif        
            break;
    }

    return state;
}

/**
 * - behaviour of the in_stopped-state depending on the position of the 
 * physical rocker implemented as a state machine
 */
static UciState in_stopped(RockerState rocker)
{

}

/**
 * - behaviour of the in_up_started-state depending on the position of the 
 * physical rocker implemented as a state machine
 * - sets the SIMPLE_UP-state or the COMFORT_UP-state depending on the time
 * difference
 */
static UciState in_up_started(RockerState rocker)
{

}

/**
 * - behaviour of the in_simple_up-state depending on the position of the 
 * physical rocker implemented as a state machine
 */
static UciState in_simple_up(RockerState rocker)
{

}

/**
 * - behaviour of the in_comfort_up-state depending on the position of the 
 * physical rocker implemented as a state machine
 */
static UciState in_comfort_up(RockerState rocker)
{

}

/**
 * - behaviour of the in_down_started-state depending on the position of the 
 * physical rocker implemented as a state machine
 * - sets the SIMPLE_DOWN-state or the COMFORT_DOWN-state depending on the time
 * difference
 */
static UciState in_down_started(RockerState rocker)
{

}

/**
 * - behaviour of the in_simple_down-state depending on the position of the 
 * physical rocker implemented as a state machine
 */
static UciState in_simple_down(RockerState rocker)
{

}

/**
 * - behaviour of the in_comfort_down-state depending on the position of the 
 * physical rocker implemented as a state machine
 */
static UciState in_comfort_down(RockerState rocker)
{

}
